# **Pesca** #
A Cytoscape app for calculating Shortest paths between nodes

**Main Features**

* Multi Shortest Paths Tree (All the shortest paths from one node to all the others)

* Multi shortest Paths (The cluster of the shortest paths between two or more nodes)

* Connect isolated nodes (Find the shortest paths from one node to a giant component)

* Download human interactome (Several human PPI network can be download to extract subnetworks from a list of proteins)

* Works with undirect, direct and weighted networks


http://apps.cytoscape.org/apps/pesca30